package hello;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ContractRestServiceApplication.class)
public abstract class BaseClass {

    @Autowired
    private EvenOddController evenOddController;

    @BeforeEach
    public void setup() {
        RestAssuredMockMvc.standaloneSetup(evenOddController);
//
//        Mockito.when(personService.findPersonById(1L))
//                .thenReturn(new Person(1L, "foo", "bee"));
    }
}