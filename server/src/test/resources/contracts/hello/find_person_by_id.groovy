package contracts.hello

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "should return Even"

    request {
        url("/validate/prime-number") {
            queryParameters {
                parameter("number", "2")
            }
        }
        method GET()
    }

    response {
        body("Even")
        status OK()
    }
}